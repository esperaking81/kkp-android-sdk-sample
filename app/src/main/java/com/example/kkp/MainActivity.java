package com.example.kkp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.NotNull;

import co.opensi.kkiapay.KKiapayCallback;
import co.opensi.kkiapay.MomoPay;
import co.opensi.kkiapay.STATUS;
import co.opensi.kkiapay.uikit.Kkiapay;
import co.opensi.kkiapay.uikit.SdkConfig;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Kkiapay.init(this, "xxxxxxxxxxxxxxxxxxxxxxx", new SdkConfig(
                R.drawable.ic_app_logo, R.color.colorPrimary, false
        ));
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Kkiapay.get().setListener(new Function2<STATUS, String, Unit>() {
            @Override
            public Unit invoke(STATUS status, String s) {
                Toast.makeText(MainActivity.this, "Transaction: " + status + " -> " + s, Toast.LENGTH_LONG).show();
                return null;
            }
        });

        findViewById(R.id.pay_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Kkiapay.get().requestPayment(MainActivity.this, "1", "Paiement de services", "KKP", "97000000");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Kkiapay.get().handleActivityResult(requestCode, resultCode, data);
    }
}